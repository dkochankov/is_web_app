package com.example.IS_WEB_APP;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IsWebAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(IsWebAppApplication.class, args);
	}

}
